FROM docker.elastic.co/elasticsearch/elasticsearch:7.5.1
MAINTAINER wangkun23 "845885222@qq.com"

ADD elasticsearch-analysis-ik-7.5.1 /usr/share/elasticsearch/plugins/elasticsearch-analysis-ik-7.5.1
